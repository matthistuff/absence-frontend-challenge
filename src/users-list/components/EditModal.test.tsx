import React from 'react';
import EditModal from "./EditModal";
import {fireEvent, render, screen} from '@testing-library/react';

const setup = (errors: any[] = []) => {
    const data = {
            "id": 1,
            "firstName": "User",
            "lastName": "Name",
            "email": "user@name.io",
            "location": 1,
            "role": 1,
            "status": 0
        },
        locations = [{
            id: 1,
            name: 'Test location'
        }],
        roles = [{
            id: 1,
            name: 'Tester'
        }],
        userStatus = [{
            id: 0,
            name: 'active'
        }],
        close = jest.fn(),
        save = jest.fn();


    render(<EditModal data={data} errors={errors} open={true} roles={roles} locations={locations}
                      userStatus={userStatus} close={close} save={save}/>);

    return {
        close,
        save
    }
}

test('should render all fields', () => {
    setup();

    expect(screen.getByLabelText('First Name')).toHaveValue('User');
    expect(screen.getByLabelText('Last Name')).toHaveValue('Name');
    expect(screen.getByLabelText('Email')).toHaveValue('user@name.io');
    expect(screen.getByLabelText('Email')).toHaveValue('user@name.io');
    expect(screen.getByLabelText('Location')).toHaveValue('1');
    expect(screen.getByLabelText('Role')).toHaveValue('1');
});

test('should pass data on save', () => {
    const mocks = setup();

    fireEvent.change(
        screen.getByLabelText('First Name'),
        {
            target: {
                value: 'NewUser'
            }
        }
    );

    fireEvent.click(screen.getByText('Save'));

    expect(mocks.save.mock.calls.length).toBe(1);
    expect(mocks.save.mock.calls[0][0]).toEqual({
        "id": 1,
        "firstName": "NewUser",
        "lastName": "Name",
        "email": "user@name.io",
        "location": 1,
        "role": 1,
        "status": 0
    });
});

test('should call the close callback', () => {
    const mocks = setup();

    fireEvent.click(screen.getByText('Close'));

    expect(mocks.close.mock.calls.length).toBe(1);
});

test('Should show an error dialog', () => {
    setup([{}]);

    expect(screen.getByText(/An error happened/)).toBeInTheDocument();
});

import React from 'react';

export interface ErrorAlertProps {
    text?: string;

    retry(): any;
}

function ErrorAlert({retry, text = 'Error while loading data!'}: ErrorAlertProps) {
    return (
        <div className="alert alert-danger" role="alert">
            {text}
            <button className="btn btn-secondary float-end" onClick={retry}>Retry</button>
        </div>
    );
}

export default ErrorAlert;

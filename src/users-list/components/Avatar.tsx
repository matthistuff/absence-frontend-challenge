import styles from '../styles/usersList.module.scss';
import {UserObject} from "../../globalTypes";

export interface AvatarProps {
    user: UserObject;
}

function Avatar({user}: AvatarProps) {
    return <span className={styles.avatar}>
        {user.firstName.substr(0, 1)}
        {user.lastName.substr(0, 1)}
    </span>
}

export default Avatar;

import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import ErrorAlert from './ErrorAlert';

test('should call home when retry button is clicked', () => {
    const onRetry = jest.fn();

    render(<ErrorAlert retry={onRetry}/>);

    fireEvent.click(
        screen.getByText('Retry')
    );

    expect(onRetry.mock.calls.length).toBe(1);
});

test('should display custom error message', () => {
    const onRetry = jest.fn();

    render(<ErrorAlert retry={onRetry} text={"Custom error!"}/>);

    expect(screen.getByText('Custom error!')).toBeInTheDocument();
})

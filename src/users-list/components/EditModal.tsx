import React, {ChangeEvent, useEffect, useState} from 'react';
import Modal from '../../ui/Modal';
import {UserObject} from "../../globalTypes";
import ErrorAlert from "./ErrorAlert";

export interface EditModalProps {
    data: UserObject;
    errors: any[];
    open: boolean;
    roles: any[];
    locations: any[];
    userStatus: any[];

    close(event: React.MouseEvent<HTMLButtonElement>): void;
    save(data: object): void;
}

function EditModal({data, errors, open, roles, locations, userStatus, close, save}: EditModalProps) {
    const [dataItem, setDataItem] = useState(data);

    useEffect(() => {
        setDataItem(data)
    }, [data]);

    const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
        const newState: any = {...dataItem};
        newState[e.target.name] = e.target.value;
        setDataItem(newState);
    }

    const saveHandler = () => {
        save(dataItem);
    }
    return (
        <Modal open={open} close={close} title="Edit User" saveTitle="Save" save={saveHandler}>
            {errors.length > 0 &&
            <ErrorAlert retry={saveHandler} text="An error happened, no detailed information yet!"/>}
            <form>
                <div className="mb-3 row">
                    <div className="col-auto">
                        <label htmlFor="userFirstName" className="form-label">First Name</label>
                        <input type="text" className="form-control" id="userFirstName" name="firstName"
                               value={dataItem.firstName}
                               onChange={handleChange} required={true}/>
                    </div>
                    <div className="col-auto">
                        <label htmlFor="userLastName" className="form-label">Last Name</label>
                        <input type="text" className="form-control" id="userLastName" name="lastName"
                               value={dataItem.lastName}
                               onChange={handleChange} required={true}/>
                    </div>
                </div>
                <div className="mb-3">
                    <label htmlFor="userEmail" className="form-label">Email</label>
                    <input type="email" className="form-control" id="userEmail" name="email"
                           value={dataItem.email}
                           onChange={handleChange} required={true}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="userRole" className="form-label">Role</label>
                    <select value={dataItem.role} onChange={handleChange} id="userRole" name="role"
                            className="form-select" required={true}>
                        {roles.map((role) => <option key={role.id} value={role.id}>{role.name}</option>)}
                    </select>
                </div>
                <div className="mb-3">
                    <label htmlFor="userLocation" className="form-label">Location</label>
                    <select value={dataItem.location} onChange={handleChange} id="userLocation" name="location"
                            className="form-select" required={true}>
                        {locations.map((location) => <option key={location.id}
                                                             value={location.id}>{location.name}</option>)}
                    </select>
                </div>
            </form>
        </Modal>
    );
}

export default EditModal;

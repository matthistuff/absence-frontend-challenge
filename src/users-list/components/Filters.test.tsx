import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import Filters from './Filters';

test('should call home when input changes', () => {
    const onChange = jest.fn();

    render(<Filters setGlobalFilter={onChange} globalFilter=""/>);

    fireEvent.input(
        screen.getByLabelText('User'),
        {
            target: {
                value: 'search'
            }
        }
    );

    expect(onChange.mock.calls.length).toBe(1);
    expect(onChange.mock.calls[0][0]).toBe('search');
});

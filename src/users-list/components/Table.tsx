import React from 'react';
import {
    TableInstance,
    TableState,
    useGlobalFilter,
    UseGlobalFiltersInstanceProps,
    UseGlobalFiltersState,
    useTable,
} from 'react-table'
import Filters from "./Filters";

export interface TableProps {
    columns: any;
    search: string;
    data: object[];

    select(item: Object): any;
}

export interface FilterTableInstance extends TableInstance<object>, UseGlobalFiltersInstanceProps<object> {
    state: FilterTableState;
}

export interface FilterTableState extends TableState<object>, UseGlobalFiltersState<object> {
}

function Table({columns, search, data, select}: TableProps) {
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        state,
        prepareRow,
        setGlobalFilter,
    } = useTable({
        columns,
        data,
    }, useGlobalFilter) as FilterTableInstance


    return (
        <>
            <Filters globalFilter={state.globalFilter} setGlobalFilter={setGlobalFilter}/>
            <table {...getTableProps([
                {
                    className: "table table-hover align-middle"
                }
            ])}>
                <thead>
                {headerGroups.map(headerGroup => (
                    <tr {...headerGroup.getHeaderGroupProps()}>
                        {headerGroup.headers.map(column => (
                            <th {...column.getHeaderProps()}>{column.render('Header')}</th>
                        ))}
                    </tr>
                ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                {rows.map((row, i) => {
                    prepareRow(row)
                    return (
                        <tr {...row.getRowProps()}
                            onClick={() => select(row.original)}>
                            {row.cells.map(cell => {
                                return <td {...cell.getCellProps({
                                    // @ts-ignore
                                    className: cell.column.className
                                })}>{cell.render('Cell')}</td>
                            })}
                        </tr>
                    )
                })}
                </tbody>
            </table>
        </>
    )
}

export default Table;

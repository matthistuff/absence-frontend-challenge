import React, {ChangeEvent, useState} from 'react';
import styles from '../styles/usersList.module.scss';
import {FilterValue} from "react-table";

export interface FiltersProps {
    setGlobalFilter: (filterValue: FilterValue) => void;
    globalFilter: string;
}

function Filters({setGlobalFilter, globalFilter}: FiltersProps) {
    const [value, setValue] = useState(globalFilter || '')

    const onChange = (event: ChangeEvent<HTMLInputElement>) => {
        setValue(event.target.value);
        setGlobalFilter(event.target.value);
    }

    return (
        <div className={styles.filters}>
            <label htmlFor="usersListSearch" className="form-label">User</label>
            <input type="search" className="form-control" id="usersListSearch"
                   placeholder="Search user" onChange={onChange} value={value}/>
        </div>
    );
}

export default Filters;

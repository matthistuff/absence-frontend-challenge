import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import Table from './Table';

test('should render a table', () => {
    const onSelect = jest.fn(),
        columns = [
            {
                Header: 'User',
                accessor: 'name'
            },
            {
                Header: 'Role',
                accessor: 'role'
            }
        ],
        data = [
            {
                id: 1,
                name: 'User 1',
                role: 'Role 1'
            },
            {
                id: 2,
                name: 'User 2',
                role: 'Role 2'
            }
        ];

    render(<Table select={onSelect} columns={columns} data={data} search=""/>);

    expect(screen.getByText('User 1')).toBeInTheDocument();
    expect(screen.getByText('User 2')).toBeInTheDocument();
});

test('should call home when a record is selected', () => {
    const onSelect = jest.fn(),
        columns = [
            {
                Header: 'User',
                accessor: 'name'
            },
            {
                Header: 'Role',
                accessor: 'role'
            }
        ],
        data = [
            {
                id: 1,
                name: 'User 1',
                role: 'Role 1'
            },
            {
                id: 2,
                name: 'User 2',
                role: 'Role 2'
            }
        ];

    render(<Table select={onSelect} columns={columns} data={data} search=""/>);

    fireEvent.click(
        screen.getByText('User 1')
    );

    expect(onSelect.mock.calls.length).toBe(1);
    expect(onSelect.mock.calls[0][0]).toEqual({
        id: 1,
        name: 'User 1',
        role: 'Role 1'
    });
});

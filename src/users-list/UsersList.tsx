import React from 'react';
import styles from './styles/usersList.module.scss';
import EditModal from './components/EditModal';
import Table from "./components/Table";
import usersApi from "../api/users";
import ErrorAlert from "./components/ErrorAlert";
import {UseTableCellProps} from "react-table";
import locationApi from "../api/locations";
import rolesApi from "../api/roles";
import statusApi from "../api/status";
import Avatar from "./components/Avatar";
import {UserObject} from "../globalTypes";

export interface UsersListProps {
    apiEndpoint: string;
}

export interface UsersListState {
    editModalOpen: boolean;
    editData: UserObject,
    editErrors: any[],
    dataSet: UserObject[];
    loading: boolean;
    error: boolean;
    search: string;
    roles: any[]
    locations: any[]
    userStatus: any[]
}

const createMap = (data: any[]) => {
    const map = new Map();

    data.forEach((item) => map.set(item.id, item.name));

    return map;
}

class UsersList extends React.Component<UsersListProps, UsersListState> {
    constructor(props: any) {
        super(props);
        this.state = {
            editModalOpen: false,
            editData: {
                id: 0,
                firstName: '',
                lastName: '',
                email: '',
                location: 0,
                role: 0,
                status: 0
            },
            editErrors: [],
            dataSet: [],
            loading: true,
            error: false,
            search: '',
            roles: [],
            locations: [],
            userStatus: []
        };
    }

    componentDidMount() {
        this.loadData();
    }

    loadData() {
        this.fetchData().then(
            (responses: any[]) => {
                const dataSet = responses[0].data,
                    locations = responses[1].data,
                    roles = responses[2].data,
                    userStatus = responses[3].data;


                this.setState({
                    loading: false,
                    error: false,
                    dataSet,
                    roles,
                    locations,
                    userStatus
                });
            },
            () => this.setState({error: true})
        );
    }

    fetchData(): Promise<any[]> {
        const users = usersApi(this.props.apiEndpoint),
            locations = locationApi(this.props.apiEndpoint),
            roles = rolesApi(this.props.apiEndpoint),
            status = statusApi(this.props.apiEndpoint);

        return Promise.all([
            users.get(),
            locations.get(),
            roles.get(),
            status.get()
        ]);
    }

    loadingFailed() {

    }

    handleEditUser(item: UserObject) {
        this.setState({
            editModalOpen: true,
            editData: item,
            editErrors: []
        });
    }

    handleSaveUser(data: UserObject) {
        const users = usersApi(this.props.apiEndpoint);

        users.update(data).then(
            async (response) => {
                const data = response.data;

                // This is to normalize the string from the form, not necessary with real endpoints.
                data.location = +data.location;
                data.role = +data.role;
                data.userStatus = +data.userStatus;

                this.setState({
                    dataSet: this.state.dataSet.map((item: UserObject): UserObject => {
                        return item.id !== data.id ? item : data;
                    }),
                    editModalOpen: false
                });
            },
            (response) => {
                this.setState({
                        editErrors: [
                            {
                                field: 'firstName',
                                error: 'Some weird error'
                            }
                        ]
                    }
                );
            }
        )
    }

    closeEditModal(e: React.MouseEvent<HTMLButtonElement>) {
        this.setState({
            editModalOpen: false,
        });
    }

    onSearch(text: string) {
        this.setState({
            search: text
        });
    }

    render() {
        const locationMap = createMap(this.state.locations),
            roleMap = createMap(this.state.roles),
            statusMap = createMap(this.state.userStatus);

        const columns = [
            {
                Header: 'User',
                accessor: (row: UserObject) => `${row.firstName} ${row.lastName}`,
                Cell: ({row}: UseTableCellProps<any>) => {
                    return <><Avatar user={row.original}/>{row.original.firstName} {row.original.lastName}</>
                }
            },
            {
                Header: 'Location',
                accessor: (row: UserObject) => locationMap.get(row.location),
                className: 'text-muted'
            },
            {
                Header: 'Role',
                accessor: (row: UserObject) => roleMap.get(row.role),
                className: 'text-muted'
            },
            {
                Header: 'User Access',
                accessor: (row: UserObject) => statusMap.get(row.status),
                className: 'text-muted'
            }
        ];

        return (
            <div className={styles['users-list']}>
                {this.state.error && <ErrorAlert retry={() => this.loadData()}/>}

                <EditModal data={this.state.editData}
                           errors={this.state.editErrors}
                           open={this.state.editModalOpen}
                           close={(e: React.MouseEvent<HTMLButtonElement>) => this.closeEditModal(e)}
                           save={(data: UserObject) => this.handleSaveUser(data)}
                           roles={this.state.roles}
                           locations={this.state.locations}
                           userStatus={this.state.userStatus}/>

                <h1>
                    Users
                    <button className="btn btn-primary float-end" disabled>Add User</button>
                </h1>

                <div className={styles.wrap}>
                    {
                        this.state.loading
                            ? <p>Loading...</p>
                            : <Table search={this.state.search} data={this.state.dataSet} columns={columns}
                                     select={(item: UserObject) => this.handleEditUser(item)}/>
                    }
                </div>
            </div>
        );
    }
}

export default UsersList;

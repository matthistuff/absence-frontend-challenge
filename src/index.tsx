import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.scss';
import UsersList from './users-list/UsersList';

declare global {
    interface Window {
        PUBLIC_URL: string;
    }
}

ReactDOM.render(
    <React.StrictMode>
        <UsersList apiEndpoint={window.PUBLIC_URL + /api/}/>
    </React.StrictMode>,
    document.getElementById('usersList')
);

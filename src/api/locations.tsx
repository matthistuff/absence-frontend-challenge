const locationApi = (apiRoot: string) => {
    return {
        get: (): Promise<any> => {
            return fetch(apiRoot + 'location.json').then((response) => response.json());
        }
    }
}

export default locationApi;

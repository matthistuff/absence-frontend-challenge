import {UserObject} from "../globalTypes";

const usersApi = (apiRoot: string) => {
    return {
        get: (): Promise<any> => {
            return fetch(apiRoot + 'user.json').then((response) => response.json());
        },

        update: (data: UserObject): Promise<any> => {
            return new Promise(
                (resolve, reject) => {
                    Math.random() < 0.5
                        ? resolve({data})
                        : reject();
                }
            )
        }
    }
}

export default usersApi;

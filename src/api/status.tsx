const statusApi = (apiRoot: string) => {
    return {
        get: (): Promise<any> => {
            return fetch(apiRoot + 'userStatus.json').then((response) => response.json());
        }
    }
}

export default statusApi;

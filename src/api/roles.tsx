const rolesApi = (apiRoot: string) => {
    return {
        get: (): Promise<any> => {
            return fetch(apiRoot + 'role.json').then((response) => response.json());
        }
    }
}

export default rolesApi;

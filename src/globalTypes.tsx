export interface UserObject extends Object {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    location: number;
    role: number;
    status: number;
}

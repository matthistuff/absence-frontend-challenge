import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import Modal from "./Modal";

const setup = (isOpen: boolean = false): any => {
    const close = jest.fn(),
        save = jest.fn();

    render(<Modal open={isOpen} title="Modal Title" saveTitle="Save" close={close} save={save}><p
        data-testid="children">Children</p></Modal>);

    return {
        close,
        save
    };
}

test('should render the Modal', () => {
    setup();

    expect(screen.getByText('Modal Title')).toBeInTheDocument();
    expect(screen.getByText('Save')).toBeInTheDocument();
    expect(screen.getByTestId('children')).toBeInTheDocument();
});

test('should call home when saving the modal', ()=>{
    const mocks = setup();

    fireEvent.click(
        screen.getByText('Save')
    );

    expect(mocks.save.mock.calls.length).toBe(1);
});

test('should call home when closing the modal', ()=>{
    const mocks = setup();

    fireEvent.click(
        screen.getByText('Close')
    );

    expect(mocks.close.mock.calls.length).toBe(1);
});

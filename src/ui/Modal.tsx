import React, {FunctionComponent, useEffect, useRef} from 'react';
import {Modal as BsModal} from 'bootstrap';

export interface ModalProps {
    open: boolean;
    title: string;
    saveTitle: string;

    close(event: React.MouseEvent<HTMLButtonElement>): any;

    save(event: React.MouseEvent<HTMLButtonElement>): any;
}

const Modal: FunctionComponent<ModalProps> = ({open, title, close, save, saveTitle, children}) => {
    const modalRef = useRef<HTMLDivElement>(null);
    useEffect(() => {
        if (modalRef.current) {
            let bsModalComponent = BsModal.getInstance(modalRef.current as HTMLDivElement)
            if (!bsModalComponent) {
                bsModalComponent = new BsModal(modalRef.current as HTMLDivElement);
            }

            open ? bsModalComponent.show() : bsModalComponent.hide();
        }
    });

    return (
        <div className="modal fade" tabIndex={-1} aria-hidden="true" ref={modalRef}>
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{title}</h5>
                        <button type="button" className="btn-close" onClick={close} aria-label="Close"/>
                    </div>
                    <div className="modal-body">
                        {children}
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" onClick={close}>Close</button>
                        <button type="button" className="btn btn-primary" onClick={save}>{saveTitle}</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Modal;

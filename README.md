# Absence Frontend Challenge

Hello Absence, this is my entry to the frontend challenge :) I had a busy week and ran out of time but have fun anyway!

## Project structure and dependencies

`src` contains everything interesting. The project specific parts are in the `users-list` folder. Everything else
could/should be part of a bigger/existing product specific framework.

Bootstrap 5 is used as UI framework because the mockups looked very bootstrappy and I know my way around this framework.

I used react-table because I wanted to try it out as it seems to come in handy for more complex table interaction. I
noticed you do filtering/sorting on the server side but included it anyway.

The build process is very heavy on dependencies but should work with a very minimal webpack setup too. I didn't have
enough time to optimize this.

## Assumptions

* Is running as part of a wider UI ecosystem, so I expect core styles (here Bootstrap 5) and module specific styles (as
  a css module)
* Backend API implementation is not part of the project, so the API is a mock using fetch to files. Saving a user fails
  randomly.

## Caveats

* User status is missing from edit screen
* Field validation is missing from edit screen
* No column sorting on the table
* Search resets when saving a record
* No internationalization
* No test for the UserList
* The CRA build is not optimized, might not have needed to eject

Straight from CRA:

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more
information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
